#include "CellCycle.hpp"

#ifndef INCLUDE_CELL_HPP
#define INCLUDE_CELL_HPP

class Cell {
    phase_e phase;
    int phase_time;
    bool death_mark;
  public:
    Cell(phase_e, int, bool);
    void mark();
    void set_phase(phase_e, int);
    bool next();

    phase_e get_phase() { return phase; }
    int get_phase_time() { return phase_time; }
    bool is_marked() { return death_mark; }
};

#endif