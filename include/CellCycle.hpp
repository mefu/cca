#include <random>

#ifndef INCLUDE_CELL_CYCLE_HPP
#define INCLUDE_CELL_CYCLE_HPP

using namespace std;

enum phase_e { 
  G1 = 0, 
  S, 
  G2, 
  M 
};

class CellCycle {
    int ls[4];
    mt19937_64* ralg;
    double v;
    double dr;
  public:
    CellCycle(int, int, int, int, double);
    int len(phase_e);
    int rlen(phase_e);
    phase_e next(phase_e);
    bool death_roll();
};

#endif