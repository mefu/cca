#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")

# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

# create build and bin directory in case it does not exist
mkdir -p ${SCRIPTPATH}/../build
mkdir -p ${SCRIPTPATH}/../bin

# go to build directory
cd ${SCRIPTPATH}/../build

# run cmake for generation
cmake ../ -G"CodeBlocks - Unix Makefiles"

# run make for compilation
make