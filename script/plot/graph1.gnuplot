# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
unset key

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../../output/plot/graph1.svg"
set key out vert right box

# Plot 1 - 1
set title "Numbers of cells in specific cycles - 30 days"
set xlabel "Time (min)"
set ylabel "Number of cells"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
plot  "../../output/data/data.dat" using 1:2 with lines title 'G1' lt rgb "#e41a1c", \
      "../../output/data/data.dat" using 1:3 with lines title 'S' lt rgb "#377eb8", \
      "../../output/data/data.dat" using 1:4 with lines title 'G2' lt rgb "#4daf4a", \
      "../../output/data/data.dat" using 1:5 with lines title 'M' lt rgb "#984ea3"