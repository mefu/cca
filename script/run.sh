#!/bin/bash

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")

# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

# create output directories in case it does not exist
mkdir -p ${SCRIPTPATH}/../output/data
mkdir -p ${SCRIPTPATH}/../output/plot

# Run program
${SCRIPTPATH}/../bin/cca $1 > ${SCRIPTPATH}/../output/data/data.dat

# Change directory into plot scripts
cd ${SCRIPTPATH}/../script/plot

# run gnuplot
gnuplot graph1.gnuplot