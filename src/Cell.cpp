#include "Cell.hpp"

Cell::Cell(phase_e p, int pt, bool dm) {
  phase = p;
  phase_time = pt;
  death_mark = dm;
}

void Cell::mark() {
  death_mark = true;
}

void Cell::set_phase(phase_e p, int pt) {
  phase = p;
  phase_time = pt;
}

bool Cell::next() {
  phase_time -= 1;
  return phase_time == -1;
}
