#include "CellCycle.hpp"
#include <iostream>

using namespace std;

CellCycle::CellCycle(int g1, int s, int g2, int m, double vr) {
  ls[G1] = g1;
  ls[S] = s;
  ls[G2] = g2;
  ls[M] = m;
  ralg = new mt19937_64(1l);
  v = vr;

  int total = g1 + s + g2 + m;
  dr = 1 - pow(0.5, 1.0/total);
}

int CellCycle::len(phase_e p) {
  return ls[p];
}

int CellCycle::rlen(phase_e p) {
  uniform_real_distribution<> rdist(1-v, 1+v);
  return len(p) * rdist(*ralg);
}

phase_e CellCycle::next(phase_e p) {
  if(p == G1) 
    return S;
  else if(p == S) 
    return G2;
  else if(p == G2) 
    return M;
  else  
    return G1;
}

bool CellCycle::death_roll() {
  uniform_real_distribution<> rdist(0, 1);
  return rdist(*ralg) < dr;
}