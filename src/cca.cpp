#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include "CellCycle.hpp"
#include "Cell.hpp"

using namespace std;

// class for holding log information each round
class CellLog {
  public:
    int cs[4];
    CellLog() { cs[0] = 0; cs[1] = 0; cs[2] = 0; cs[3] = 0; }
    void add(Cell c) { cs[c.get_phase()] += 1; }
    int total() { return cs[0] + cs[1] + cs[2] + cs[3]; }
    void print(int t) {
      cout << t << " " << cs[G1] << " " << cs[S] << " " << cs[G2] << 
                   " " << cs[M]  << " " << total() << endl;
    }
};

int main(int argc, char *argv[]) {
  if(argc != 2) {
    cout << "Please specify config file." << endl;
    return 1;
  }

  char* filepath = argv[1];

  std::ifstream infile(filepath);

  // parameters
  int g1, s, g2, m, icc, tmax;
  double v;

  // read parameters
  infile >> g1 >> s >> g2 >> m >> v >> icc >> tmax;

  // create cell cycle
  CellCycle cc(g1, s, g2, m, v);

  // create vector for cells
  vector<Cell> cells;

  // initialize cells
  for(int i = 0; i < icc; i++) {
    cells.push_back(*(new Cell(G1, cc.rlen(G1), false)));
  }

  for(int t = 0; t < tmax; t++) {
    // create log for current time step
    CellLog cl;

    // vector for cells in next round
    vector<Cell> ncells;

    // for each cell
    for(vector<Cell>::iterator it = cells.begin(); it != cells.end(); ++it) {
      // add to log
      cl.add(*it);

      // probability roll for death, mark if it hits
      if(cc.death_roll()) {
        (*it).mark();
      }

      // if cell dies this round or not
      bool dead = false;

      // next function decreases phase_time of cell by 1 and returns true if it is 0
      if((*it).next()) {
        // get next phase
        phase_e next_phase = cc.next((*it).get_phase());

        // if next phase is M, create a new cell
        if( next_phase == M ) {
          // new cell inherits father cells death mark
          Cell nc(G1, cc.rlen(G1), (*it).is_marked());
          ncells.push_back(nc);
        }

        // if (G1->S or G2->M) and cell is marked
        dead = (next_phase == S || next_phase == M) && (*it).is_marked();

        // update phase info of cell
        (*it).set_phase(next_phase, cc.rlen(next_phase));
      }

      // if not dead, push cell to next round cells
      if( !dead ) {
        ncells.push_back(*it);
      }

    }

    // print log
    cl.print(t);

    // update cells with cells of next round
    cells = ncells;
  }

  return 0;
}


